# Zilla Slab
A custom family for Mozilla by Typotheque


## Installation

```
virtualenv env
source env/bin/activate

pip install -r requirements.txt
```

## Generating fonts

buil whilst env virtualenv is active
```
cd sources
sh build.sh
```